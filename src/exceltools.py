from xlsxwriter import Workbook
from .report_types import ReportInfo, ReportTable, Chart, Space, CellType, HeaderCell, Header, Plate, ChartType, \
    ChartLabelType, excel_header, HeaderTitle
from typing import Tuple, List, Union

from xlsxwriter.worksheet import Worksheet


def generate_excel_report(file, report: ReportInfo):
    """ Формирует файл Excel

    :param file: Путь к файлу
    :param report: Описание отчета
    """
    use_constant_memory = report.use_optimizations
    with ReportFileGenerator(file, use_constant_memory) as g:
        for page in report.pages:
            sheet = g.add_page(page.name, column_width=page.column_width)
            row = 0  # Текущий номер строки
            y_offset = 0  # Смещение в пикселях по оси y. Используется для графиков
            max_row = None
            max_height = 0
            for line in page.layout:
                column = 0
                x_offset = 0
                # Определяем позицию для текущего уровня,
                # стоит обратить внимание, что если на предыдущем уровне были графики
                # может потребоваться ручное указание смещения в строках через объект Space
                if max_row is not None:
                    row = max_row
                    y_offset = 0  # сбрасываем смещение по оси y, поскольку графики также отсчитываются от строки, и данное смещение работать не будет
                else:
                    y_offset += max_height
                max_row = None
                max_height = 0
                if isinstance(line, Space):
                    max_row = row + line.size

                elif isinstance(line, ReportTable):
                    column += line.margin_left
                    _, row_end, column = g.add_table(table=line, page=sheet, row=row + line.margin_top, column=column)
                    column += line.margin_right
                    max_row = row_end + line.margin_bottom + 1
                elif isinstance(line, (tuple, list)):
                    for item in line:
                        if isinstance(item, Space):
                            column += item.size
                            x_offset = 0
                        elif isinstance(item, ReportTable):
                            column += item.margin_left
                            _, row_end, column = g.add_table(table=item, page=sheet, row=row + item.margin_top,
                                                             column=column)
                            column += item.margin_right
                            x_offset = 0
                            current_max_row = row_end + item.margin_bottom + 1
                            max_row = max(max_row, current_max_row) if max_row else current_max_row


class ReportFileGenerator:
    def __init__(self, file, use_constant_memory):
        self.constant_memory = use_constant_memory
        self.lang = 'ru'
        self.wb = Workbook(file, {
            'constant_memory': use_constant_memory,
            'remove_timezone': True,
        })

        self.title_format = self.wb.add_format({
            'bold': True,
            'font_name': 'Arial',
            'font_size': 18,
        })

        header_format = {
            'text_wrap': True,
            'valign': 'top',
            'border': 1,
            'font_name': 'Calibri',
            'font_size': 11,
            'bold': True,
            'font_color': '#FFFFFF',
            'bg_color': '#808080',  # 4F81BD
            'border_color': 'black',
            'pattern': 1,  # solid fill
        }

        header_format_center = header_format.copy()
        header_format_center.update({
            'valign': 'vcenter',
            'align': 'center'
        })

        self.header_format = header_format
        self.header_format_center = header_format_center
        self.cell_formats = self.generate_cell_formats()

        self.borders = {
            'topleft': self.wb.add_format({'left': 1, 'top': 1}),
            'topright': self.wb.add_format({'top': 1, 'right': 1}),
            'bottomleft': self.wb.add_format({'left': 1, 'bottom': 1}),
            'bottomright': self.wb.add_format({'bottom': 1, 'right': 1}),
            'top': self.wb.add_format({'top': 1}),
            'left': self.wb.add_format({'left': 1}),
            'bottom': self.wb.add_format({'bottom': 1}),
            'right': self.wb.add_format({'right': 1}),
        }
        self.header_get_format = {
            CellType.text: {'num_format': '@'},
            CellType.date: {'num_format': 'dd.mm.yyyy'},
            CellType.int: {'num_format': '0'},
            CellType.float: {'num_format': '0.00'},
            CellType.datetime: {'num_format': 'dd.mm.yyyy hh:mm:ss'},
            CellType.short_datetime: {'num_format': 'dd.mm.yyyy hh:mm'},
            CellType.short_time: {'num_format': '[h]:mm'},
            CellType.time: {'num_format': '[h]:mm:ss'},
            CellType.percent: {'num_format': '0.00%'},
            CellType.int_percent: {'num_format': '0%'},
            CellType.wrapped_text: {'text_wrap': True, 'valign': 'top'},
            CellType.bold_text: {'bold_text': True},
            CellType.simple_time: {'num_format': 'h:mm:ss'},
        }

    def generate_cell_formats(self):
        fmt = {
            'border_color': 'black',
            'border': 1,
            'valign': 'top',
        }
        return {
            CellType.default: self.wb.add_format(fmt.copy()),
            CellType.text: self.wb.add_format(fmt.copy()),
            CellType.int: self.wb.add_format(dict(fmt.copy(), **{'num_format': '0'})),
            CellType.float: self.wb.add_format(dict(fmt.copy(), **{'num_format': '0.00'})),
            CellType.month: self.wb.add_format(dict(fmt.copy(), **{'num_format': 'MMMM'})),
            CellType.short_day: self.wb.add_format(dict(fmt.copy(), **{'num_format': 'd'})),
            CellType.date: self.wb.add_format(dict(fmt.copy(), **{'num_format': 'dd.mm.yyyy'})),
            CellType.datetime: self.wb.add_format(dict(fmt.copy(), **{'num_format': 'dd.mm.yyyy hh:mm:ss'})),
            CellType.short_datetime: self.wb.add_format(dict(fmt.copy(), **{'num_format': 'dd.mm.yyyy hh:mm'})),
            CellType.time: self.wb.add_format(dict(fmt.copy(), **{'num_format': '[h]:mm:ss'})),
            CellType.short_time: self.wb.add_format(dict(fmt.copy(), **{'num_format': '[h]:mm'})),
            CellType.simple_time: self.wb.add_format(dict(fmt.copy(), **{'num_format': 'h:mm:ss'})),
            CellType.custom_time: self.wb.add_format(dict(fmt.copy(), **{'num_format': 'h:mm'})),
            CellType.percent: self.wb.add_format(dict(fmt.copy(), **{'num_format': '0.00%'})),
            CellType.int_percent: self.wb.add_format(dict(fmt.copy(), **{'num_format': '0%'})),
            CellType.wrapped_text: self.wb.add_format(dict(fmt.copy(), **{'text_wrap': True, 'valign': 'top'})),
            CellType.bold_text: self.wb.add_format(dict(fmt.copy(), **{'bold': True})),
        }

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def close(self):
        self.wb.close()
        self.wb = None

    def add_page(self, name='Report', column_width=None, *, hidden=False):
        page = self.wb.add_worksheet(name)
        if hidden:
            page.hide()
        if column_width:
            for i, width in enumerate(column_width):
                page.set_column(i, i, width=width)
        return page

    def _write_header(self, page: Worksheet, header_start_row: int, header_start_column: int,
                      header: Header, header_alignment: str, header_types: list):

        is_multiline = isinstance(header[0], (tuple, list))
        format = self.header_format_center if header_alignment == 'center' else self.header_format
        for row_idx, row_data in enumerate(header if is_multiline else [header]):
            for col_idx, cell in enumerate(row_data):
                if cell is None or getattr(cell, 'value', True) is None:
                    continue
                row = row_idx + header_start_row
                column = col_idx + header_start_column
                if header_types:
                    format.update(self.header_get_format[header_types[column]])
                if isinstance(cell, HeaderCell) and (cell.rows > 1 or cell.columns > 1):
                    page.merge_range(first_row=row, first_col=column,
                                     last_row=row + cell.rows - 1, last_col=column + cell.columns - 1,
                                     data=cell.value, cell_format=self.wb.add_format(format))
                elif isinstance(cell, excel_header) and (cell.rows > 1 or cell.columns > 1):
                    page.merge_range(first_row=row, first_col=column,
                                     last_row=row + cell.rows - 1, last_col=column + cell.columns - 1,
                                     data=cell.name if self.lang == 'ru' else cell.en_name, cell_format=self.wb.add_format(format))
                else:
                    if self.lang == 'ru':
                        cell_value = cell.name if isinstance(cell, excel_header) else cell
                    elif self.lang == 'en':
                        cell_value = cell.en_name if isinstance(cell, excel_header) else cell
                    page.write(row, column, cell_value, self.wb.add_format(format))

        return len(header) if is_multiline else 1

    def add_table(self, table: ReportTable, page: Worksheet, row: int, column: int) -> Tuple[int, int, int]:
        row_increment = 0
        if table.title:
            if isinstance(table.title, HeaderTitle):
                if self.lang == 'ru':
                    header_title = table.title.name
                elif self.lang == 'en':
                    header_title = table.title.en_name

                page.write(row, column, header_title, self.title_format)
            else:
                page.write(row, column, table.title, self.title_format)
            row_increment = 1 + (table.empty_rows_after_title or 0)

        table_start_row = row + row_increment
        header_start_row = table_start_row
        if table.header:
            if not table.as_native_table or self.constant_memory or table.column_merges:
                table_start_row += self._write_header(page, header_start_row, column, table.header,
                                                      table.header_alignment, table.header_types)
            else:
                table_start_row += 1  # Заголовочная строка будет записана позже, при создании таблицы

        default_format = self.cell_formats[CellType.default]
        if table.column_types:
            formats = [self.cell_formats.get(value, CellType.default) for value in table.column_types] \
                      + [default_format] * max(table.column_count - len(table.column_types), 0)
        else:
            formats = [default_format] * table.column_count

        row_number = 0
        merge_values = {}
        column_merges = (table.column_merges or {})
        if table.data:
            for row_number, row in enumerate(table.data):
                row_len = len(row)
                for j in range(table.column_count):
                    value = None if j >= row_len else row[j]
                    if table.context_types:
                        # Используются типы ячеек по значениям, ищем подходящее значение
                        for ctype, _format in table.context_types.items():
                            if isinstance(value, ctype):
                                format = self.cell_formats.get(_format, self.cell_formats[CellType.default])
                                break
                        else:
                            # TODO: странное решение, возможно имеет смысл брать формат столбца, а не формат по умолчанию
                            format = self.cell_formats[CellType.default]
                    else:
                        format = formats[j]
                    # Если используются мержи внутри столбца, то строки в столбце,
                    # содержащие одинаковое значение объединяются
                    if column_merges.get(j) and merge_values.get(j):
                        merge_start_row, merge_value, merge_format = merge_values[j]
                        if value != merge_value:
                            prev_row = row_number - 1
                            if prev_row + table_start_row > merge_start_row:
                                page.merge_range(first_row=merge_start_row,
                                                 first_col=column + j,
                                                 last_row=table_start_row + prev_row,
                                                 last_col=column + j,
                                                 data=merge_value, cell_format=merge_format)
                            merge_values[j] = None
                    if column_merges.get(j) and not merge_values.get(j):
                        merge_values[j] = table_start_row + row_number, value, format
                    if isinstance(value, dict):
                        value = value[self.lang]
                    page.write(table_start_row + row_number, column + j, value, format)
        # Выполняем объединение оставшихся столбцов
        for merge_column, value in merge_values.items():
            if value is None:
                continue
            merge_start_row, value, cell_format = value
            if row_number > merge_start_row:
                page.merge_range(first_row=merge_start_row,
                                 first_col=column + merge_column,
                                 last_row=table_start_row + row_number,
                                 last_col=column + merge_column,
                                 data=value, cell_format=cell_format)
        if table.data:
            row_count = row_number + 1
        else:
            row_count = row_number
        if table.header:
            make_table = not self.constant_memory and not table.column_merges and table.as_native_table \
                         and (isinstance(table.header, (list, tuple)) or table.header.height == 1)
            if make_table:
                page.add_table(first_row=header_start_row,
                               first_col=column,
                               last_row=header_start_row + row_count,
                               last_col=column + table.column_count - 1,
                               options={
                                   'header_row': True,
                                   'columns': [{'header': ' ' if title == '' else title,
                                                'header_format': self.header_format} for title in table.header],
                                   'style': 'TableStyleLight9',
                               })
            elif table.as_native_table or table.with_autofilter:
                page.autofilter(first_row=table_start_row - 1,
                                first_col=column,
                                last_row=table_start_row - 1 + row_count,
                                last_col=column + table.column_count - 1)
        return row_count, table_start_row + row_count - 1, column + table.column_count - 1