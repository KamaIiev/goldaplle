from collections import namedtuple
from typing import Optional, List, Any, Iterable, Union, Dict
from enum import Enum


class TimeIntervalType(Enum):
    day = 0
    month = 1
    week = 2
    year = 3


class CellType(Enum):
    default = 'default'
    title = 'title'
    header = 'header'
    text = 'text'
    bold_text = 'bold_text'
    wrapped_text = 'wrapped_text'
    int = 'int'
    float = 'float'
    date = 'date'
    datetime = 'datetime'
    short_datetime = 'short_datetime'
    time = 'time'
    short_time = 'short_time'
    percent = 'percent'
    int_percent = 'int_percent'
    custom_time = 'custom_time'
    simple_time = 'simple_time'
    month = 'month'
    short_day = 'short_day'


class ChartType(Enum):
    bar = 'bar'
    pie = 'pie'
    doughnut = 'doughnut'
    column = 'column'
    stacked = 'bar|stacked'
    column_stacked = 'column|stacked'


HeaderCell = namedtuple('HeaderCell', ['value', 'rows', 'columns'])

HeaderItem = Union[HeaderCell, str, None]
Header = Optional[Union[List[List[HeaderItem]], List[HeaderItem]]]


class excel_header:
    def __init__(
            self,
            name,
            en_name='',
            multi_header: bool = False,
            rows: int = 0,
            columns: int = 0,
    ):
        self._name = name
        self._rows = rows
        self._columns = columns
        self._en_name = en_name
        self._multi_header = multi_header

    @property
    def name(self):
        return self._name

    @property
    def rows(self):
        return self._rows

    @property
    def columns(self):
        return self._columns

    @property
    def en_name(self):
        return self._en_name

    @property
    def multi_header(self):
        return self._multi_header


class HeaderTitle:
    def __init__(self, name, en_name):
        self._name = name
        self._en_name = en_name

    @property
    def name(self):
        return self._name

    @property
    def en_name(self):
        return self._en_name

    @property
    def get_titles(self):
        return {'ru': self._name, 'en': self._en_name}


# TODO : проверить не разумнее ли использовать set для column_merges
class ReportTable:
    __slots__ = ['_column_count', '_data', '_title', '_header', '_column_types',
                 '_as_native_table', '_with_autofilter', '_empty_rows_after_title', '_margin_top',
                 '_margin_left', '_margin_bottom', '_margin_right', '_context_types', '_column_merges',
                 '_header_alignment', '_header_types']

    def __init__(self, *,
                 column_count: int,
                 header_types: list = None,  # Кастомизация header-type
                 data: Iterable[Any],
                 title: str = None,
                 header: Header = None,
                 column_types: Optional[List[Optional[CellType]]] = None,
                 as_native_table: bool = True,
                 with_autofilter: bool = False,
                 context_types: Optional[Dict[Any, CellType]] = None,
                 column_merges: Optional[Dict[int, bool]] = None,
                 empty_rows_after_title: int = 1,
                 margin_top: int = 0,  # отступ сверху (до заголовка) в строках
                 margin_left: int = 0,  # отступ слева в столбцах
                 margin_bottom: int = 0,  # отступ снизу в строках (до следующей таблицы)
                 margin_right: int = 0,  # отступ справа с толбцах
                 header_alignment: str = 'default'):  # горизонтальное выравнивание текста в заголовке, варианты default и center (судя по коду они идентичны)
        """ Используется для описания добавляемой таблицы
        :param column_count: Количество столбцов
        :param data: Данные для таблицы
        :param title: Заголовок
        :param header: Строка или строки с заголовками столбцов таблицы
        :param column_types: Типы данных столбцов
        :param as_native_table: Представлять таблицу, как таблицу Excel
        :param with_autofilter: Добавлять фильтры к столбцам (не требуется если as_native_table == True)
        :param context_types: маппинг типов данных python в типы Excel, нужно если в одном столбце могут быть значения разных типов
        :param column_merges: Указывает в каких столбцах объединять строки по одинаковым значениям
        :param empty_rows_after_title: Количество пустых строк после заголовка
        :param margin_top: Отступ до таблицы (включая заголовок) сверху в строках
        :param margin_left: Отступ до таблицы слева в столбцах
        :param margin_bottom: Отступ после таблицы снизу в строках
        :param margin_right: Отступ после таблицы справа в столбцах
        :param header_alignment: Горизонтальное выравнивание значений строки заголовка, Значния: default, center.
        """
        self._title = title
        self._header = header
        self._column_types = column_types
        self._column_count = column_count
        self._as_native_table = as_native_table
        self._with_autofilter = with_autofilter
        self._data = data
        self._empty_rows_after_title = empty_rows_after_title
        self._context_types = context_types
        self._column_merges = column_merges
        self._margin_top = margin_top
        self._margin_left = margin_left
        self._margin_bottom = margin_bottom
        self._margin_right = margin_right
        self._header_alignment = header_alignment
        self._header_types = header_types

    @property
    def header_types(self): return self._header_types

    @property
    def title(self): return self._title

    @property
    def header(self): return self._header

    @property
    def column_types(self): return self._column_types

    @property
    def column_count(self): return self._column_count

    @property
    def as_native_table(self): return self._as_native_table

    @property
    def with_autofilter(self): return self._with_autofilter

    @property
    def empty_rows_after_title(self):
        return self._empty_rows_after_title

    @property
    def context_types(self): return self._context_types

    @property
    def column_merges(self): return self._column_merges

    @property
    def margin_top(self): return self._margin_top

    @property
    def margin_left(self): return self._margin_left

    @property
    def margin_bottom(self): return self._margin_bottom

    @property
    def margin_right(self): return self._margin_right

    @property
    def data(self): return self._data

    @property
    def header_alignment(self): return self._header_alignment


class ChartLabelType(Enum):
    Value = 'value'
    Category = 'category'
    Percentage = 'percentage'


class ChartLabel:
    __slots__ = ['_types', '_leader_lines', '_separator', '_custom']

    def __init__(self, *, types: Optional[List[ChartLabelType]] = None, separator: str = ';',
                 leader_lines: bool = False, custom: List[Any] = None):
        """ Описывает подписи к вершинам графиков
        :param types: Типы значений, которые появляются в подписях - значения, категория и процент. Можно комбинировать.
        :param separator: Разделитель типов подписей
        :param leader_lines: Отображать или нет линии до подписей. Актуально для круговой диаграммы
        :param custom: Свои подписи к каждой вершине. Описание: https://xlsxwriter.readthedocs.io/working_with_charts.html#chart-series-option-data-labels. Раздел: Chart series option: Custom Data Labels
        """
        self._types = types
        self._separator = separator
        self._leader_lines = leader_lines
        self._custom = custom

    types = property(lambda self: self._types)
    separator = property(lambda self: self._separator)
    leader_lines = property(lambda self: self._leader_lines)
    custom = property(lambda self: self._custom)


class Chart:
    __slots__ = ['_id', '_title', '_type', '_data', '_column_count', '_column_types', '_width', '_height',
                 '_margin_top', '_margin_left', '_margin_bottom', '_margin_right', '_legend_position',
                 '_data_label', '_plotarea', '_with_header', '_y_min', '_y_max', '_x_min', '_x_max']

    def __init__(self,
                 id: str,
                 title: str,
                 type: ChartType,
                 data: Iterable[Any],
                 column_count: int,
                 column_types: List[CellType],
                 width: Optional[int] = None,
                 height: Optional[int] = None,
                 margin_top: int = 5,
                 margin_left: int = 5,
                 margin_right: int = 5,
                 margin_bottom: int = 5,
                 legend_position: Optional[str] = None,
                 data_label: Optional[Union[List[ChartLabel], ChartLabel]] = None,
                 plotarea: Optional[tuple] = None,
                 with_header: bool = False,
                 y_min: Optional[int] = None,
                 y_max: Optional[int] = None,
                 x_min: Optional[int] = None,
                 x_max: Optional[int] = None):
        """ Используется для описания добавляемого графика
        :param id: Уникальный идентификатор в рамках Excel файла
        :param title: Заголовок графика
        :param type: Тип графика
        :param data: Данные для графика
        :param column_count: Количество столбцов(наборов) данных для графика
        :param column_types: Список с типом каждого набора данных
        :param width: Ширина графика в пикселях
        :param height: Высота графика в пикселях
        :param margin_top: отступ сверху в пикселях
        :param margin_left: отступ слева в пикселях
        :param margin_right: отступ справа в пикселях
        :param margin_bottom: отступ снизу в пикселях
        :param legend_position: положение легенды. Значения bottom, left, top, right, top_right (xlsxwriter/chart.py:2733),
            None - отсутствие легенды
        :param data_label: Метки на вершинах графика
        :param plotarea: кортеж значений x,y,width,height описывающих область графика.
            Подробности: https://xlsxwriter.readthedocs.io/chart.html#set_plotarea
        :param with_header: Признак - является ли первый элемент поля data заголовком данных
        :param y_min: минимальное значение оси ординат
        :param y_max: максимальное значение оси ординат
        :param x_min: минимальное значение оси абцисс
        :param x_max: максимальное значение оси абцисс
        """
        self._id = id
        self._title = title
        self._type = type
        self._data = data
        self._column_count = column_count
        self._column_types = column_types
        self._width = width
        self._height = height
        self._margin_top = margin_top
        self._margin_left = margin_left
        self._margin_bottom = margin_bottom
        self._margin_right = margin_right
        self._legend_position = legend_position
        self._data_label = data_label
        self._plotarea = plotarea
        self._with_header = with_header
        self._y_min = y_min
        self._y_max = y_max
        self._x_min = x_min
        self._x_max = x_max

    @property
    def id(self): return self._id

    @property
    def title(self): return self._title

    @property
    def type(self): return self._type

    @property
    def column_count(self): return self._column_count

    @property
    def column_types(self): return self._column_types

    @property
    def width(self): return self._width

    @property
    def height(self): return self._height

    @property
    def margin_top(self): return self._margin_top

    @property
    def margin_left(self): return self._margin_left

    @property
    def margin_bottom(self): return self._margin_bottom

    @property
    def margin_right(self): return self._margin_right

    @property
    def data(self): return self._data

    @property
    def legend_position(self):
        return self._legend_position

    @property
    def data_label(self):
        return self._data_label

    @property
    def plotarea(self):
        return self._plotarea

    @property
    def with_header(self):
        return self._with_header

    @property
    def y_min(self):
        return self._y_min

    @property
    def y_max(self):
        return self._y_max

    @property
    def x_min(self):
        return self._x_min

    @property
    def x_max(self):
        return self._x_max


class Plate:
    __slots__ = ['_data', '_title', '_column_type', '_width', '_height',
                 '_empty_rows_after_title', '_margin_top', '_margin_left',
                 '_margin_bottom', '_margin_right']

    def __init__(self, *,
                 data: Iterable[Any],
                 title: str = None,
                 column_type: Optional[CellType] = None,
                 width: Optional[int] = None,
                 height: Optional[int] = None,
                 empty_rows_after_title: int = 1,
                 margin_top: int = 0,
                 margin_left: int = 0,
                 margin_bottom: int = 0,
                 margin_right: int = 0):
        self._data = data
        self._title = title
        self._column_type = column_type
        self._width = width
        self._height = height
        self._empty_rows_after_title = empty_rows_after_title
        self._margin_top = margin_top
        self._margin_left = margin_left
        self._margin_bottom = margin_bottom
        self._margin_right = margin_right

    @property
    def data(self):
        return self._data

    @property
    def title(self):
        return self._title

    @property
    def column_type(self):
        return self._column_type

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    @property
    def empty_rows_after_title(self):
        return self._empty_rows_after_title

    @property
    def margin_top(self):
        return self._margin_top

    @property
    def margin_left(self):
        return self._margin_left

    @property
    def margin_bottom(self):
        return self._margin_bottom

    @property
    def margin_right(self):
        return self._margin_right


Space = namedtuple('Space', ['size'])

PageItem = Union[ReportTable, Chart, Space, Plate]
Layout = Union[List[Union[PageItem, List[PageItem]]], Chart]


class Page:
    __slots__ = ['_name', '_layout', '_column_width', '_is_image', '_csv', '_title']

    def __init__(self, name: str, *, column_width: List[int] = None,
                 layout: Layout, is_image: bool = False, csv: bool = True, title: str = '') -> object:
        """ Лист Excel

        :param name: Имя листа
        :param column_width: Список ширин столбцов
        :param layout: Элементы листа
        """
        self._name = name
        self._column_width = column_width
        self._layout = layout
        self._is_image = is_image
        self._csv = csv
        self._title = title

    @property
    def name(self): return self._name

    @property
    def layout(self): return self._layout

    @property
    def column_width(self): return self._column_width

    @property
    def is_image(self): return self._is_image

    @property
    def csv(self): return self._csv

    @property
    def title(self): return self._title


class ReportInfo:
    __slots__ = ['_pages', '_use_optimizations', '_csv', '_openpyxl', '_template']

    def __init__(self,
                 pages: List[Page],
                 use_optimizations: bool = False,
                 csv: bool = True,
                 openpyxl: bool = False,
                 template: str = None):
        """ Описание отчет
        :param pages: список листов
        :param use_optimizations: Использовать или нет оптимизированный режим для больших отчетов
        :param: csv: создавать ли csv
        :param: openpyxl: генерация отчета через openpyxl
        :param: template: путь до шаблона
        """
        self._pages = pages
        self._use_optimizations = use_optimizations
        self._csv = csv
        self._openpyxl = openpyxl
        self._template = template

    @property
    def pages(self): return self._pages

    @property
    def use_optimizations(self): return self._use_optimizations

    @property
    def csv(self): return self._csv

    @property
    def openpyxl(self): return self._openpyxl

    @property
    def template(self): return self._template
