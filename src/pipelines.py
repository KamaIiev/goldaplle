# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import datetime
import json

# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

from .report_types import ReportInfo, Page, ReportTable, CellType
from .exceltools import generate_excel_report
from . import spiders
from src.items import LetaItem2


def get_report_info_1(data):
    types = [CellType.text] * 3 + [CellType.int] * 2 + [CellType.text] + [CellType.date]
    width = [10] * 2 + [20] + [10] * 10
    header = ["Бренд", "Сайт", "Конкурент ссылка", "Цена до скидки", "Цена со скидкой или по карте лояльности",
              "Доступен для заказа (есть остаток)", "Дата"]
    return ReportInfo(use_optimizations=False,
                      pages=[Page('Report',
                                  column_width=width,
                                  layout=[ReportTable(column_count=len(types),
                                                      as_native_table=False,
                                                      with_autofilter=False,
                                                      data=data,
                                                      title='',
                                                      header=header,
                                                      header_alignment='center',
                                                      column_types=types)

                                          ]
                                  )])


def get_report_info_2(data):
    types = [CellType.text] * 5 + [CellType.date]
    width = [10] * 2 + [20] + [10] * 10
    header = ["Наименование на сайте", "Ссылка","Доступен для заказа (есть остаток)", "Цена до скидки", "Цена со скидкой или по карте лояльности", "Дата"]
    return ReportInfo(use_optimizations=False,
                      pages=[Page('Report',
                                  column_width=width,
                                  layout=[ReportTable(column_count=len(types),
                                                      as_native_table=False,
                                                      with_autofilter=False,
                                                      data=data,
                                                      title='',
                                                      header=header,
                                                      header_alignment='center',
                                                      column_types=types)

                                          ]
                                  )])


class SrcPipeline:
    def __init__(self):
        self.map = {
            spiders.leta1_spider.LetaSpider: self.process_leta_1,
            spiders.leta2_spider.LetaSpider2: self.process_leta_2,
            spiders.leta3_spider.LetaSpider3: self.process_leta_2,
        }

        self.report_map = {
            spiders.leta1_spider.LetaSpider: get_report_info_1,
            spiders.leta2_spider.LetaSpider2: get_report_info_2,
            spiders.leta3_spider.LetaSpider3: get_report_info_2,
        }

    def open_spider(self, spider):
        self.items = []

    def close_spider(self, spider):
        time = round(datetime.datetime.now().timestamp())
        func = self.report_map.get(spider.__class__)
        generate_excel_report(f'{spider.name}_{time}.xlsx', func(self.items))

    def process_item(self, item, spider):
        func = self.map.get(spider.__class__)
        if func:
            return func(item)

        return item

    def process_leta_1(self, item):
        check = ['vendor', 'host', 'url', 'old_price', 'base_price', 'cart']
        self.items.append([item[i] for i in check] + [datetime.datetime.now().date()])
        return item

    def process_leta_2(self, item: LetaItem2):
        self.items.append([item.product, item.url, item.base_price, item.old_price, item.status, datetime.datetime.now().now()])
        return item
