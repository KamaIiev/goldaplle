# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.
from . import utils
from .leta1_spider import LetaSpider
from .leta2_spider import LetaSpider2
from .leta3_spider import LetaSpider3