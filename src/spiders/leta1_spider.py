import os
from pathlib import Path
import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy_selenium import SeleniumRequest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from . import utils
import pathlib
from bs4 import BeautifulSoup

tmp = os.path.join(pathlib.Path(__file__).parent.resolve(), 'tmp')


def get_element(selector, xpath):
    el = selector.xpath(xpath).get()
    if el:
        el = el.strip()
    return el


class FileNotFound(Exception):
    pass


class LetaSpider(scrapy.Spider):
    name = 'leta'

    def __init__(self, file, *args, **kwargs):
        super().__init__(**kwargs)
        self.items = None
        self.file = os.path.join(os.getcwd(), file)
        if not os.path.isfile(self.file):
            raise FileNotFound(self.file)

    def start_requests(self):
        self.items = utils.parse_file(self.file)
        # self.items = [i for i in self.items][:40]
        for item in self.items:
            yield SeleniumRequest(
                cb_kwargs={'item': item},
                url=item.url,
                callback=self.parse,
                screenshot=True,
                wait_time=10,
                script='window.scrollTo(0, document.body.scrollHeight);',
                wait_until=EC.element_to_be_clickable(
                    (By.XPATH,
                     '/html/body/div[1]/div[1]/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/div/div/svg/use'))
            )

    def parse(self, response, **kwargs):
        item = kwargs['item']
        soup = BeautifulSoup(response.text, 'lxml')

        price_block = soup.find("div", {"class": "product-detail-price"})
        data = {}
        if price_block:
            for tag, cls, step in [('div','product-detail-price__old', 'old_price'), ('span','product-detail-price__base-price', 'base_price'), ('div', 'product-detail-price__discount', 'discount')]:
                element = price_block.find(tag, {"class": cls})
                data[step] = element.get_text(strip=True) if element else element
            if data.get('old_price') is None:
                data['old_price'] = data['base_price']
                data['base_price'] = None
        cart = soup.find('button', {'class', 'le-button product-detail-cart__btn le-button--theme-primary le-button--size-lg le-button--with-label le-button--fluid'})
        # Path(f'{tmp}/{item.id}.html').write_bytes(response.body)
        # with open(f'{tmp}/{item.id}.png', 'wb') as image_file:
        #     image_file.write(response.meta['screenshot'])
        yield {'vendor': item.vendor, 'host': item.host, 'url': response.url, 'base_price': data.get('base_price'), 'old_price': data.get('old_price'),'cart': "+" if cart else "-"} #todo rewrite on common dataclass


if __name__ == '__main__':
    process = CrawlerProcess()
    process.crawl(LetaSpider, file='/Users/macbookair/PycharmProjects/goldAplle/Задание для Разработчик.xlsx')
    process.start()
