import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy_selenium import SeleniumRequest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from . import utils
import pathlib
from bs4 import BeautifulSoup
from src.items import LetaItem2


class LetaSpider3(scrapy.Spider):
    name = 'leta3'

    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        self.url = 'https://www.letu.ru'

    def start_requests(self):
        yield SeleniumRequest(
            url='https://www.letu.ru/browse/uhod-za-volosami/konditsionery-i-balzamy',
            callback=self.parse_links,
            screenshot=False,
            wait_time=10,
            wait_until=EC.element_to_be_clickable(
                (By.CLASS_NAME,
                 'products-group-content'))
        )

    def parse_links(self, response, **kwargs):
        soup = BeautifulSoup(response.text, 'lxml')

        links = soup.findAll('a', {'class': 'le-pagination__link'})
        tmp = []
        for link in links:
            link = link.get_text(strip=True)
            print(link)
            try:
                tmp.append(int(link))
            except ValueError as e:
                continue

        for i in range(min(tmp), max(tmp) + 1):
            yield SeleniumRequest(
                url=f'https://www.letu.ru/browse/uhod-za-volosami/konditsionery-i-balzamy/page-{i}',
                callback=self.parse,
                screenshot=False,
                wait_time=10,
                wait_until=EC.element_to_be_clickable(
                    (By.CLASS_NAME,
                     'products-group-content'))

            )

    def parse(self, response, **kwargs):

        soup = BeautifulSoup(response.text, 'lxml')

        group = soup.find('div', {'class': 'products-group-content'})
        products = group.findAll('div', {'class': 'product-tile results-listing-content__item'})

        # le-pagination__item le-pagination__item--next
        for product in products:
            item = LetaItem2()
            actual_price = product.find('span', {'class': 'product-tile-price__text product-tile-price__text--actual'})
            status = product.find('span', {'class': 'product-tile-price__text'})

            old_price = product.find('span', {'class': 'product-tile-price__text product-tile-price__text--old'})
            names = product.find('div', {'class': 'product-tile__wrapper product-tile__wrapper--name'}).findAll('span')[
                -1]
            product_url = product.find('a', {'class': 'product-tile__item-container'}).get('href')
            if actual_price:
                item.base_price = actual_price.get_text(strip=True)

            if old_price:
                item.old_price = old_price.get_text(strip=True)

            if names:
                item.product = names.get_text(strip=True)

            if product_url:
                item.url = f'{self.url}{product_url}'

            if status:
                status = status.get_text(strip=True)
                item.status = status if status == 'Нет в наличии' else 'В наличии'

            if status == 'Нет в наличии':
                yield SeleniumRequest(
                    cb_kwargs={'item': item},
                    url=f'https://www.letu.ru/brand/payot{product_url}',
                    callback=self.parse_product,
                    screenshot=True,
                    wait_time=10,
                    wait_until=EC.element_to_be_clickable(
                        (By.CLASS_NAME,
                         'product-detail-price'))
                )
            yield item

    def parse_product(self, response, **kwargs):
        item = kwargs['item']
        soup = BeautifulSoup(response.text, 'lxml')

        price_block = soup.find("div", {"class": "product-detail-price"})
        if price_block:
            for tag, cls, step in [('div', 'product-detail-price__old', 'old_price'),
                                   ('span', 'product-detail-price__base-price', 'base_price')]:
                element = price_block.find(tag, {"class": cls})
                setattr(item, step, element.get_text(strip=True) if element else element)
        yield item
