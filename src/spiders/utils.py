from typing import NamedTuple, List, Set
import requests
import pandas as pd


class ExcelCell(NamedTuple):
    id: int
    vendor: str
    host: str
    url: str

    def __hash__(self):
        return hash(self.url)


def parse_file(path: str) -> Set[ExcelCell]:
    xl = pd.ExcelFile(path)
    df = xl.parse('Задание 1')
    items = []
    for index, row in df.iterrows():
        items.append(
            ExcelCell(
                id=index,
                vendor=row['Бренд'],
                url=row['Конкурент ссылка'],
                host=row['Сайт']
            )
        )

    return set(items)
