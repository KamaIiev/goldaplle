# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html
import dataclasses

import scrapy


class SrcItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


@dataclasses.dataclass
class ResultCell:
    product: str
    price: str
    full_price: str
    url: str


@dataclasses.dataclass
class LetaItem2:
    product: str = ''
    url: str = ''
    status: str = ''
    base_price: str = ''
    old_price: str = ''
